#!/bin/sh
# (rpm)trigger script for okas
# $Id$
. /usr/lib/okas/functions
umask 002

case "$1" in
vim)
	# in PLD, vimrc is present earlier than vim itself when installing vim
	# for the first time. so check fixed location rc file first.
	if [ -f /etc/vim/vimrc ]; then
		vimrc=/etc/vim/vimrc
	fi

	# Ubuntu
	if [ -f /etc/vim/vimrc.local ]; then
		vimrc=/etc/vim/vimrc.local
	fi

	if [ -z "$vimrc" ]; then
		vim=$(which vim 2>/dev/null)
		[ "$vim" ] || exit 0

		vimrc=$(vim --version 2>&1 | awk '/system vimrc/ {print $NF}' | xargs)
	fi

	# Ubuntu: see if vimrc loads /etc/vim/vimrc.local
	if [ -f "$vimrc" ] && grep -q 'source /etc/vim/vimrc.local' "$vimrc"; then
		vimrc=/etc/vim/vimrc.local
		# create it
		if [ ! -f "$vimrc" ]; then
			touch "$vimrc"
		fi
	fi

	if [ "$vimrc" ]; then
		PatchFile "$vimrc" \
			'"?.*so.*vim/okas\.vim' \
			's|^"\?.*so.*vim/okas\.vim.*$|so /usr/share/okas/vim/okas.vim|g' \
			'so /usr/share/okas/vim/okas.vim'
	fi
;;


bashrc)
	# bashrc
	if [ -f /etc/bash/bashrc ]; then # Gentoo
		bashrc=/etc/bash/bashrc

	elif [ -f /etc/bash.bashrc ]; then # Debian/Ubuntu
		bashrc=/etc/bash.bashrc

	elif [ -f /etc/bash.bashrc ]; then # SuSE
		bashrc=/etc/bash.bashrc.local
		# file is not present on livecd
		touch $bashrc

	elif [ -f /etc/bashrc.local -a -f /etc/bashrc ] && grep -q /etc/bashrc.local /etc/bashrc; then # okas
		bashrc=/etc/bashrc.local

	else
		bashrc=/etc/bashrc
	fi

	if [ -d /etc/shrc.d ]; then # PLD
		ln -sf /usr/share/okas/bashrc /etc/shrc.d/okas.sh
		if [ -L /etc/shrc.d/local.sh ] && [ ! -L /etc/shrc.d/~local.sh ]; then
			mv /etc/shrc.d/local.sh /etc/shrc.d/~local.sh
		fi

		if [ -f /etc/bashrc.local ] && [ ! -L /etc/shrc.d/~local.sh ]; then
			ln -s ../bashrc.local /etc/shrc.d/~local.sh
		fi

		# remove old include
		UnPatchFile $bashrc \
			's|^.*okas/bashrc|# &|g'
	else
		if [ -f $bashrc ]; then
			PatchFile $bashrc \
				'#?.*okas/bashrc' \
				's|^#\?.*okas/bashrc.*$|test "$PS1" -a "$BASH_VERSION" \&\& . /usr/share/okas/bashrc|g' \
				'test "$PS1" -a "$BASH_VERSION" && . /usr/share/okas/bashrc'
		fi
	fi
;;

sysctl.conf)
	[ -f /etc/sysctl.conf ] || exit 0
	[ "`uname -s`" = Linux ] || exit 0

	# hack /etc/sysctl.conf
	for key in `sed -e '/^#/d;/^$/d;s/ .*//' /usr/share/okas/sysctl.conf`; do
		grep -q "^[# ]*$key" /etc/sysctl.conf && continue
		sed -e '/./{H;$!d;}' -e "x;/$key/!d" /usr/share/okas/sysctl.conf >> /etc/sysctl.conf
	done

;;


rpm.macros)
	# RPM tune
	if [ -d /var/lib/rpm ]; then
		[ ! -d /etc/rpm ] && mkdir -m 755 /etc/rpm

		if [ -f /etc/rpm/macros.lang ]; then
			# skip if /etc/rpm/macros has anything to do with %_install_langs
			if ! grep -q '%_install_langs' /etc/rpm/macros; then
				# skip if something already enabled
				if ! grep -q '^%_install_langs' /etc/rpm/macros.lang; then
					# enable _install_langs only when it was commented out. don't force
					if grep -q '^#%_install_langs' /etc/rpm/macros.lang; then
						sed -i -e 's|^#.*\(%_install_langs\)|\1|g' /etc/rpm/macros.lang
					fi
				fi
			fi
		else
			macros=/etc/rpm/macros
			[ ! -f $macros ] && touch $macros

			# enable _install_langs only when it was commented out. don't force
			PatchFile $macros \
				'^#%_install_langs.*$' \
				's|^#.*\(%_install_langs\)|\1|g' \
				'%_install_langs et_EE:lv_LV:lt_LT:ru_RU:en_US'
		fi
	fi
;;


XTerm|XFree86)
	# XTerm App-Defaults
	[ -f /usr/X11R6/lib/X11/app-defaults/XTerm ] || exit 0
	PatchFile \
		/usr/X11R6/lib/X11/app-defaults/XTerm \
		'!?.*#include.*okas/XTerm.ad"' \
		's|^!\?.*#include.*okas/XTerm.ad".*$|#include "/usr/share/okas/XTerm.ad"|g' \
		'#include "/usr/share/okas/XTerm.ad"'

;;

yum)
	for dir in /etc/yum.repos.d /etc/yum/repos.d; do
		if [ -d $dir ]; then
			ln -sf /usr/share/okas/yum.repo $dir/okas.repo
			exit 0
		fi
	done
;;

poldek)
	[ -d /etc/poldek/repos.d ] || exit 0
	ln -sf /usr/share/okas/poldek.repo /etc/poldek/repos.d/okas.conf
;;

apt)
	[ -d /etc/apt/sources.list.d ] || exit 0
	ln -sf /usr/share/okas/apt.repo /etc/apt/sources.list.d/okas.list
;;

subversion)
	[ -d /etc/subversion ] || exit 0
	[ ! -e /etc/subversion/config ] || exit 0
	ln -sf /usr/share/okas/subversion /etc/subversion/config
;;
esac
