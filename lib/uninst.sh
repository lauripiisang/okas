#!/bin/sh
# preuninstall script for okas
# $Id$
. /usr/lib/okas/functions
umask 002

case "$1" in
XTerm|XFree86)
	# XTerm App-Defaults
	UnPatchFile \
		/usr/X11R6/lib/X11/app-defaults/XTerm \
		's|.*#include.*okas/XTerm.ad".*$|! &|g'
;;

bashrc)
	# bashrc
	if [ -f /etc/bash.bashrc ]; then # SuSE
		bashrc=/etc/bash.bashrc.local
	elif [ -f /etc/bash.bashrc ]; then # Debian/Ubuntu
		bashrc=/etc/bash.bashrc
	elif [ -f /etc/bashrc.local ]; then # okas
		bashrc=/etc/bashrc.local
	else
		bashrc=/etc/bashrc
	fi

	UnPatchFile $bashrc \
		's|^.*okas/bashrc|# &|g'

	# PLD
	if [ -L /etc/shrc.d/okas.sh ]; then
		rm -f /etc/shrc.d/okas.sh
	fi
;;

vim)
	UnPatchFile /etc/vim/vimrc \
		's|.*so.*vim/okas.vim|" &|g' \
;;

yum)
	[ -d /etc/yum/repos.d ] || exit 0
	if [ -L /etc/yum/repos.d/okas.repo ]; then
		rm -f /etc/yum/repos.d/okas.repo
	fi
;;

poldek)
	[ -d /etc/poldek/repos.d ] || exit 0
	if [ -L /etc/poldek/repos.d/okas.conf ]; then
		rm -f /etc/poldek/repos.d/okas.conf
	fi
;;

apt)
	[ -d /etc/apt/sources.list.d ] || exit 0
	if [ -L /etc/apt/sources.list.d/okas.list ]; then
		rm -f /etc/apt/sources.list.d/okas.list
	fi
;;

subversion)
	[ -d /etc/subversion ] || exit 0
	if [ -L /etc/subversion/config ] && [ $(readlink /etc/subversion/config) = /usr/share/okas/subversion ]; then
		rm -f /etc/subversion/config
	fi
;;

esac
