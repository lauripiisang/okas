# fg hack, which calls xtitle if foregrounded job begins with 'xapp '
# source this from your bashrc
#
# Author: glen@delfi.ee
# Date: 06/01/2001
# $Id$

function fg() {
	local args="$@"
	local rex

	case "$args" in
	[0-9]*) # 'fg <num>'
		rex='^\['$args'\]'
	;;
	%[0-9]*) # 'fg %<num>'
		rex='^\['${args#?}'\]'
	;;
	%*) # 'fg %<name>'
		rex='^\[[0-9]+\].{27}'"${args#?}"
	;;
	'') # 'fg' no args - last job
		rex='^\[[0-9]+\]\+'
	;;
	'+') # 'fg +' - last job
		rex='^\[[0-9]+\]\+'
	;;
	'-') # 'fg -' - prev job
		rex='^\[[0-9]+\]-'
	;;
	*) # 'fg <name>' - any junk
		rex='^\[[0-9]+\].{27}'"$args"
	;;
	esac

	local match=`jobs | egrep "$rex"`
	# multiple matches don't count - bash says it's ambigious
	if [ "$match" -a `echo "$match" | wc -l` = 1 ]; then
		match=`echo "$match" | sed -e 's,^[\[0-9\]\+].\{27\},,;s,$,,;s,  (wd: .*)$,,;s, \&$,,'`
		case "$match" in
		'xapp '*)
			xtitle "${TITLE}: ${match#xapp?}"
			;;
		esac
	fi
	builtin fg "$@"
}

alias '%'=fg
