" add this link to your system vimrc, if not present:
" so <sfile>:p:h/glen_vimrc.vim

" few defaults
set ts=4 sw=4 nowrap title is noai backup ic
set ruler	" show the cursor position all the time

" Switch syntax highlighting on, when the terminal has colors
" Also switch on highlighting the last used search pattern.
if &t_Co > 2 || has("gui_running")
	syntax on
	set hlsearch
endif

if has("eval")
	let is_kornshell = 1
	" user can customize her/his $TITLE from environment
	let &titlestring = $TITLE ." - VIM: %t%(\ %M%)%(\ (%{expand(\"%:p:h\")})%)%(\ %a%)"

	" better hiliting in php code
	let php_sql_query = 1
	let php_htmlInStrings = 1
	let php_folding = 1
	let filetype_inc = "php"

	let sql_type_default = "mysql"
endif

" on xterm default hlsearch is unreadable
if &term =~ "^xterm" || &term =~ "screen"
	hi Search ctermfg=red ctermbg=blue
endif

" Only do this part when compiled with support for autocommands
if has("autocmd")
	" Enable file type detection.
	" Use the default filetype settings, so that mail gets 'tw' set to 72,
	" 'cindent' is on in C files, etc.
	" Also load indent files, to automatically do language-dependent indenting.

	" do this only with vim >= 6.x that has the filetype support
	if exists(":filetype")
		filetype plugin indent on
	endif

	" In text files, limit the width of text to 78 characters, but be careful
	" that we don't override the user's setting.
	autocmd BufNewFile,BufRead *.txt if &tw == 0 | set tw=78 | endif

	" When editing a file, always jump to the last known cursor position.
	" System vimrc azzhole has done it wrong!, so remove it first (the bang)
	au! BufReadPost * if line("'\"") > 0 && line("'\"") <= line("$") | exe "normal '\"" | endif

	" don't backup crontab editings -- security risk! -glen
	" maybe don't keep backup at all in /tmp, /var/tmp ?
	autocmd BufReadPre,FileReadPre tmp/crontab*,tmp/bash*,tmp/cvs*,Dropbox/* set nobackup

	augroup filetype
		" use setf <filetype> to change syntax hiliting for specific filetype
		au BufNewFile,BufRead *.php3,*.php,*.inc,*.tmpl setf php
		au BufNewFile,BufRead *.htt setf html

		au BufNewFile,BufRead bashrc setf sh

		" mysql sql history
		au BufNewFile,BufRead .mysql_history setf mysql
		au BufNewFile,BufRead .my.cnf,my.cnf,mysqld.conf setf dosini

		" java properties
		au BufNewFile,BufRead *.props setf jproperties

		" Apache configs.
		au BufNewFile,BufRead /etc/apache/*.conf setf apache

		" When editing a crontab file, set backupcopy to yes rather than
		" auto. See :help crontab and gentoo bug #53437.
		autocmd FileType crontab set backupcopy=yes
	augroup END

	function! Cstrftime(fmt)
		let l = v:lang
		lang C
		let t = strftime(a:fmt)
		exe "lang ". l
		return t
	endfun

	au Syntax spec map <F7> /^%changelog<CR>o<CR><ESC>k:call Putline(Cstrftime("* %a %b %d %Y ". $USER ." <".$USER."@".$HOSTNAME.">"))<CR>I-

	" The cvs commit from multiple dirs feeds you with old CVSLOG,
	" that isn't catched by scripts.vim, check the last line for the '^CVS:'
	au BufReadPost * if !did_filetype() && getline(line("$")) =~ '^CVS:' | setf cvs | endif
endif " has autocommands


if has("user_commands")
	" add XXX, FIXME, TODO to some syntaxes
	au Syntax * call TODOSyntax()

	function! TODOSyntax()
		syn keyword glenTodo FIXME XXX contained
		hi def link glenTodo Todo
		exe "syn cluster ". &ft ."CommentGroup add=glenTodo"
	endfun

	" php is somewhy special ;(
	au Syntax php syn keyword phpTodo FIXME XXX contained

	function! Putline(arg)
		let res = append(line(".") - 1, a:arg)
	endfun

	function! CommentToggle()
		let v = "g:". &ft . "_comment_str"
		if exists(v)
			exec "let s = ". v
		else
			let s = "#"
		endif
		let lineno = line(".")
		let curline = getline(lineno)
		if strpart(curline, 0, strlen(s)) == s
			let res = strpart(curline, strlen(s), strlen(curline))
		else
			let res = s . curline
		endif
		call setline(line("."), res)
	endfun

	let c_comment_str = "//"
	let cpp_comment_str = "//"
	let vim_comment_str = "\""
	let css_comment_str = "//"
	let java_comment_str = "//"
	let xdefaults_comment_str = "!"
	let dosini_comment_str = ";"
	let asterisk_comment_str = ";"
	let ael_comment_str = "//"
	let cs_comment_str = "//"
	let php_comment_str = "//"

	map <Esc>\ :call CommentToggle()<CR>j
	imap <Esc>\ <ESC>:call CommentToggle()<CR>j
	map <Esc>' :call CommentToggle()<CR>j
	imap <Esc>' <ESC>:call CommentToggle()<CR>j

endif " has user_commands

" Hightlight trailing whitespace (from horde/docs/CODING_STANDARDS)
highlight BadWhitespace ctermbg=red guibg=red
match BadWhitespace /\s\+$/
" Hightlight Invisible Evil UTF8 Space 0xc2a0
match BadWhitespace /\%u00a0/

" Don't use Ex mode, use Q for formatting
map Q gq

" nice bindings for navigating between files
map <Esc>n :bnext<CR>
map <Esc>p :bprevious<CR>
map <Esc>l :ls<CR>
map <Esc>1 :buffer 1<CR>
map <Esc>2 :buffer 2<CR>
map <Esc>3 :buffer 3<CR>
map <Esc>4 :buffer 4<CR>
map <Esc>5 :buffer 5<CR>
map <Esc>6 :buffer 6<CR>
map <Esc>7 :buffer 7<CR>
map <Esc>8 :buffer 8<CR>
map <Esc>9 :buffer 9<CR>
map <Esc>0 :buffer 10<CR>
map <Esc>u :bunload<CR>
map <Esc>e :e <C-D>

" make C-n, C-p open next file
map <C-n> :n<CR>
map <C-p> :N<CR>

" make F2 save current document
map <F2> :update<CR>
imap <F2> <ESC><F2>a

" map F4 to switch between vim windows
map  <F4> <C-w><C-w><C-w>_
imap <F4> <ESC><C-w><C-w><C-w>_

" make the tab key match bracket pairs
nnoremap <tab> %
vnoremap <tab> %

" Use <C-L> to clear the highlighting of :set hlsearch.
" https://github.com/tpope/vim-sensible/blob/master/plugin/sensible.vim
if maparg('<C-L>', 'n') ==# ''
  nnoremap <silent> <C-L> :nohlsearch<C-R>=has('diff')?'<Bar>diffupdate':''<CR><CR><C-L>
endif
" clear out a search
nnoremap <leader><space> :noh<cr>

" http://vimcasts.org/episodes/show-invisibles/
" Shortcut to rapidly toggle `set list`
nmap <leader>l :set list!<CR>
" Use the same symbols as TextMate for tabstops and EOLs
" Add trail and nbsp
set listchars=tab:▸\ ,eol:¬,trail:·,nbsp:≡

" Markdown helper: duplicate current line and replace every character with '='
nnoremap <leader>1 yypVr=

" reselect the text that was just pasted
nnoremap <leader>v V`]

" open new Scratch window, https://github.com/mtth/scratch.vim
nmap <leader><tab> :Scratch<CR>

" Rainbow Parentheses, https://github.com/dbarsam/vim-rainbow-parentheses.git
nmap <leader>R :Scratch<CR>

" Gundo, http://sjl.bitbucket.org/gundo.vim/#installation
nnoremap <F5> :GundoToggle<CR>

" avoid common :X mistype. replace it with :x
" http://stackoverflow.com/a/17793858
cnoreabbrev X x
