#!/bin/awk -f
BEGIN {
	df = "df -Th";
	for (i = 1; i < ARGC; i++ ){
		df = df " " ARGV[i];
	}

	while ((df | getline) > 0) {
		if (NF < 7) {
			# extra line, buffer it
			buf = $0;
			df | getline
			p = $5;
		} else {
			p = $6;
		}

		# usage without percent mark
		l = substr(p, 1, length(p) - 1);
		if (p == "-" || (p = int(p)) == l) {
			if (p == "-") {
				c = 41;
			} else if (p >= 90) {
				c = 31;
			} else if (p >= 80) {
				c = 33;
			} else {
				c = 32;
			}

			if (buf) {
				printf ("\033[%sm%s\033[0m\n", c, buf);
			}
			printf ("\033[%sm%s\033[0m\n", c, $0);
		} else {
			# any other line
			printf("%s\n", $0);
		}
		buf = "";
	}
	close(df);
	exit(0);
}
