@echo off
:: $Id$
:: rtar .bat wrapper for win32 rtar.sh
:: make shortcut in SentTo\ folder and make it point to something like:
:: %SystemDrive%\cygwin\usr\local\bin\rtar.bat hellsgate.online.ee
::
:: Author: glen@delfi.ee
:: Date: 9/8/2002

set CMDARGS=%*%
set FILENAME=%3
%SystemDrive%\cygwin\bin\sh.exe -c "/usr/local/bin/rtar-cygwin.sh '%1'"
