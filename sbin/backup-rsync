#!/bin/sh
# Author: glen@delfi.ee
# Requires: rsync, fileutils

PROGRAM="${0##*/}"

set -e

die() {
	echo >&2 "$PROGRAM: $*"
	exit 1
}

usage() {
	cat << EOF
Usage: $PROGRAM [options]

Backup files incrementally using rsync,
each set is full dump, while the most recent is BASE.0

  -h|--help             display this help and exit.
     --src=DIR          directory what to shapshot.
                        [default=$src]
     --dst=DIR          directory where to store snapshots.
                        [default=$dst]
     --versions=N       number of snapshots to keep.
                        [default=$versions]
     --name=NAME        use NAME as target directory. defaults to basename of SRC.
     --ignore-vanished  pass --ignore-errors to rsync, and ignore rsync exit code 24
     --use-subdirs      process snapshot by subdirs. makes tracking progress easier for huge dirs
  -v|--verbose          increase verbosity
  -n|--dry-run          show what would have been transferred

EOF
	exit 1
}

add_exclude() {
	[ -z "$exclude_file" ] && exclude_file=$(mktemp ${TMPDIR:-/tmp}/brsyncXXXXXX)
	echo "$1" >> "$exclude_file"
}

# calls rsync. handles extra exit code
do_rsync() {
	local rc

	rsync $verbose ${dry:+-nv} \
		${exclude_file:+--exclude-from=$exclude_file} \
		--no-whole-file \
		--timeout=7200 \
		--numeric-ids \
		--delete --delete-excluded --delete-after \
		$ignore_errors \
		"$@" && rc=$? || rc=$?

	# ignore vanished hack
	if [ "$ignore_errors" -a $rc = 24 ]; then
		rc=0
	fi

	if [ $rc != 0 ]; then
		die "rsync failed: $rc"
	fi

	return $rc
}

# destination dir where to store snapshot
dst=/srv/backup

# number of versions to keep
versions=30

# dry run
dry=

# verbosity
verbose=

# ignore rsync errors from vanished files, helpful for backuping Maildir storate where files come and go
# rsync exit code "24": Partial transfer due to vanished source files.
# https://eventum.dev.delfi.ee/view.php?id=39022
ignore_errors=

# use subdirs when doing rsync
# this makes big dirs syncing more progressive: you can get a understanding which dir is in processing
# this also lessens chance files get removed as syncing is done in smaller chunks
use_subdirs=

# parse command line args
TEMP=$(getopt -o hvn --long versions::,src::,dst::,dry-run,name::,exclude::,ignore-vanished,use-subdirs -n "$PROGRAM" -- "$@")

# Note the quotes around `$TEMP': they are essential!
eval set -- "$TEMP"

while true; do
	case "$1" in
	-h|--help)
		usage
	;;

	--dst)
		dst="$2"
		shift
	;;

	--src)
		src="$2"
		shift
	;;

	--name)
		name="$2"
		shift
	;;

	--versions)
		versions="$2"
		shift
	;;

	-n|--dry-run)
		dry=1
	;;

	-v|--verbose)
		verbose="$verbose -v"
	;;

    --ignore-vanished)
        ignore_errors="--ignore-errors"
	;;

    --use-subdirs)
        use_subdirs=1
	;;

	--exclude)
		add_exclude "$2"
		shift
	;;

	--)
		shift
		break
	;;

	*)
		die "Internal error: '$1' not recognized"
	;;
	esac

	shift
done

if [ -z "$src" -o -z "$dst" ]; then
	usage
fi

if [[ "$src" = /* ]] && [ ! -d "$src" ]; then
	die "Source dir does not exist: $src"
fi

if [ ! -d "$dst" ]; then
	die "Destination dir must exist: $dst"
fi

if [ "$name" ]; then
	base="$name"
else
	base=$(basename "$src")
fi

cd "$dst"

# skip linking for first run
if [ -d "$base.0" ]; then
	link_dest="$base.0"
fi

tmp=$(mktemp -d $base.XXXXXX) || die "Can't mktemp"

if [ "$use_subdirs" ]; then
	# first copy top level files and dirs itself
	do_rsync \
		-dl \
		${link_dest:+--link-dest="../$link_dest"} \
		"$src/" "$tmp/"

	# preserve permissions of top dir
	if [ "$link_dest" ]; then
		chmod --reference ${link_dest} $tmp/
	fi

	for dir in "$tmp"/*/; do
		dir=${dir%*/} # remove trailing slash
		dir=${dir##*/} # get basename
		do_rsync \
	   		-rlptD \
			${link_dest:+--link-dest="../../$link_dest/$dir"} \
			"$src/$dir/"  "$tmp/$dir/" "$@" || rc=$? && rc=$?
	done
else
	do_rsync \
		-rlptD \
		${link_dest:+--link-dest="../$link_dest"} \
		"$src/" "$tmp/" "$@" || rc=$? && rc=$?
fi

if [ -z "$dry" ]; then
	rm -rf "$base.$((versions-1))"
	for v in $(seq $versions -1 1); do
		s=$(($v - 1))
		if [ -d "$base.$s" ]; then
			mv -f "$base.$s" "$base.$v"
		fi
	done
	mv "$tmp" "$base.0"
fi

if [ "$exclude_file" ]; then
	rm -f "$exclude_file"
fi
