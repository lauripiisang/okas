#!/bin/sh
PROGRAM=${0##*/}
set -e

# aborts program abnormally
die() {
	local rc=${2:-1}
	if [ "$1" ]; then
		echo >&2 "$PROGRAM: ERROR: $1"
	fi
	exit $rc
}

msg() {
	echo >&2 "$*"
}

gethostname() {
	local host

	[ "$host" ] || host=$(hostname -s 2>/dev/null)
	[ "$host" ] || host=$(hostname -f 2>/dev/null)
	[ "$host" ] || host=$(hostname 2>/dev/null)

	host=${host%%.*}

	[ "$host" ] || die "Can't resolve hostname, hostname not found"

	echo $host
}

getdomainname() {
	local host=$(hostname -f 2>/dev/null)

	[ "$host" ] || host=$(hostname 2>/dev/null)
	[ "$host" ] || die "Can't resolve domain, hostname not found"

	local domain=${host##*.}

	[ "$domain" != "$host" ] || die "Couldn't resolve domain, $host is without domain suffix"
	echo $domain
}

# checks if $1 contains spaces
has_spaces() {
	local input="$1"

	case "$input" in *' '*) return 1 ;; esac
	return 0
}

host=$(gethostname) || die
domain=$(getdomainname) || die

has_spaces "$host" || die "hostname can't contain spaces: hostname='$host'"
has_spaces "$domain" || die "hostname can't contain spaces: domain='$domain'"

force=
if [ "$1" = "--force" ]; then
	force=1
else
	force=0
fi

msg "Initializing CVS configuration for $host, domain: $domain"

if [ -d /etc/CVS ]; then
	die "$host already configured for CVS; remove /etc/CVS to re-init?"
fi

# test if config already exists in CVS (machine re-install)
t=$(mktemp -d /tmp/srvcvsinitXXXXXX) || die
cd $t || die

cvs co -l -d $host servers/$domain/root/$host 2>/dev/null || :
if [ -d $t/$host ]; then
	if [ "$force" != 1 ]; then
		rm -rf $t
		die "$host configuration already in cvs, use --force to checkout."
	fi
fi

cvs co -l -d root servers/$domain/root
cd root
mkdir $host
cvs add $host
cd $host
mkdir etc
cvs add etc
cp -a etc/CVS /etc
msg "CVS dir created to /etc"

rm -rf $t
