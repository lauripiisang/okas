#!/usr/bin/perl -w
# rpm package updater
# derivered from rpm-upgrade
# maintainer: glen <glen@delfi.ee>
#
# TODO:
# XXX use archpref in case of multiple rpms
# XXX order by release if multiple releases present and choose only one pkg
#     update: the remote dir has only i package listed in packages.info

$| = 1;
my $VERSION = sprintf("%s", q$Id$ =~ /(\d+\.\d+)/);
my $RPMDIR = ".";

use Getopt::Long;
use strict;

use vars qw(@noinstall); 

# read local config
require '/etc/rpm-upgrade.conf' if -r '/etc/rpm-upgrade.conf';

my @archpref = ('i686', 'i585', 'i386');
push(@noinstall, qw(
	kernel kernel-enterprise kernel-BOOT kernel-doc kernel-headers kernel-ibcs kernel-pcmcia-cs kernel-smp kernel-source kernel-utils
));

my %opts = ();
my $debug = 1;


GetOptions(\%opts, "noinstall=s@", "options=s@");

push @noinstall, split(/,/, join(',', @{$opts{'noinstall'}})) if $opts{'noinstall'};
my %qhash = map { $_ => 1} @noinstall;

my %installed;

print "upgrade v$VERSION\n";

print "Reading all installed packages....";
open(PKGS,"rpm -qa --qf '%{NAME} %{VERSION} %{RELEASE} %{BUILDTIME} %{ARCH}\n'|") || die "Cannot read packages: $!";

while(<PKGS>) {
	chomp;
	my ($name, $ver, $rel, $bt, $arch)=split;

#	print "Name: $name $ver $arch (".localtime($bt).")\n" if $debug;
	$installed{$name}="$bt-$ver-$rel";
}
close(PKGS);
print "Done.\n";

my $URL = shift if (@ARGV);

unless ($URL) {
	$URL = 'http://netiva.online.ee/updates';
	if (-f '/etc/redhat-release' and `grep 'Red Hat Linux release 7\.0' /etc/redhat-release`) {
		$URL .= '/7.0';
	} elsif (-f '/etc/redhat-release' and `grep 'Red Hat Linux release 7\.1' /etc/redhat-release`) {
		$URL .= '/7.1';
	} elsif (-f '/etc/redhat-release' and `grep 'Red Hat Linux release 7\.2' /etc/redhat-release`) {
		$URL .= '/7.2';
	} elsif (-f '/etc/redhat-release' and `grep 'Red Hat Linux release 7\.3' /etc/redhat-release`) {
		$URL .= '/7.3';
	} elsif (-f '/etc/pld-release' and `grep 'PLD' /etc/pld-release`) {
		$URL .= '/pld';
	}
}
die "No url?\n" unless $URL;

my $noinstall = 0;
my $dontinstall = 0;
my $newer = 0;
my $notinstalled = 0;
my @upgrade;

print "Reading packagelist from $URL/packages.info\n";
my @pkglist;
open(PIPE, "wget -q -O - $URL/packages.info |") || die "Cannot read packages: $!";
@pkglist = <PIPE>;
close(PIPE);

# retry with new url
if ($pkglist[0] =~ m#^Location: (.+)/packages\.info$#) {
	print "Location: $1\n";
	$URL = $1;
	open(PIPE, "wget -q -O - $URL/packages.info |") || die "Cannot read packages: $!";
	@pkglist = <PIPE>;
	close(PIPE);
}

unless (@pkglist) {
	print "Reading packagelist from $URL\n";
	open(PIPE, "wget -q -O - $URL/ |") || die "Cannot read packages: $!";
	my @here = sort map {/HREF="(.*)">/; $1} grep {m/\.rpm/} <PIPE>;
	close(PIPE);

	my @urllist = map {"$URL/$_"} @here;

	open (PKGS, "rpm -qp --qf '%{NAME} %{VERSION} %{RELEASE} %{BUILDTIME} %{ARCH}\n' ". join(" ", @urllist) . "|") || die "Cannot read packages: $!";
	@pkglist = <PKGS>;
	close(PKGS);
}

my @forced;
foreach (@pkglist) {
	my ($name, $ver, $rel, $bt, $arch) = split;

	my $f = quotemeta $name;
	if ( grep {m/^$f$/} @ARGV ) {
		print "forced install $name ($ver-$rel.$arch)\n";
		push(@forced, "$name-$ver-$rel.$arch.rpm");
		next;
	}

	if (defined $installed{$name}) {
		if (defined $qhash{$name}) {
			$dontinstall++;
			next;
		}
		if ((my $cmval = mycompare( [split(/\D+/, "$bt-$ver-$rel")], [split(/\D+/, $installed{$name})] )) > 0) {
			print "upgrade $name ($ver-$rel.$arch)\n";
			push(@upgrade, "$name-$ver-$rel.$arch.rpm");
		} else {
			if ($cmval < 0) {
				$newer++;
			} else {
				$noinstall++;
			}
		}
	} else {
		$notinstalled++;
	}
}
push (@upgrade, @_);
print "\n";
print "         Not installed: $notinstalled\n";
print "         Don't install: $dontinstall".($dontinstall?" ".join(",",keys %qhash):"")."\n";
print "      Already upgraded: $noinstall\n";
print "                 Newer: $newer\n";
print "            To upgrade: ".($#upgrade + 1)." packages\n";
print "       Forced download: ". ($#forced + 1). " packages\n" if @forced;

print "Fetching packages...\n";
my @fetch_list = map {"$URL/$_"} @upgrade, @forced;

unless (-w ".") {
	die "Current dir not writable, not fetching. fetch list is: ".join(" ", @fetch_list)."\n";
} else {
	system(qq(wget -nc -q @fetch_list));
}

sub mycompare {
	my ($i, $j) = @_;
	my $n;

#	print "comparing (".join(",",@$i).") with (".join(",",keys %qhash).")\n";

	for $n (0..(scalar @$i)-1) {
		if($$i[$n] <=> $$j[$n]) {
			return $$i[$n] <=> $$j[$n];
		}
	}
	return 0;
}

